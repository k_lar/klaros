all:
	@echo RUN \'make build\' to build klaros
	@echo RUN \'make run\' to run klaros in virtual machine \(qemu\)
	@echo RUN \'make clean\' to clean .bin files

build:
	nasm -f bin boot.asm -o boot.bin

run:
	qemu-system-x86_64 boot.bin

start: build run

clean:
	@rm boot.bin
