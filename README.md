# klarOS

My first OS that I am trying to make.

## Instructions

How to run:

```bash
# Compile asm to binary files
make build

# Run the binary files with qemu
make run
```

## Tools

- nasm
- qemu
- gcc
- dd

# Notes (Warning: There will be many)

### Assembly

- $ is the current memory address
- $$ is the beginning of the current section
- Important registers: ax, bx, cx, dx
- 0x55, 0xaa are magic boot numbers
